#-*- coding: utf-8 -*-
from django.contrib import admin
from inventory.models import Spr_location, Spr_type, Spr_unit, Spr_status, Asset, ImageSprUnit
__author__ = 'p12se'

def moveCategoryPrinter(ModelAdmin, request, queryset):
    queryset.update(id_type='1')
moveCategoryPrinter.short_description = u"Переместить в группу Принтер"

def moveCategoryMonitor(ModelAdmin, request, queryset):
    queryset.update(id_type='2')
moveCategoryMonitor.short_description = u"Переместить в группу Монитор"

def moveCategoryComputer(ModelAdmin, request, queryset):
    queryset.update(id_type='3')
    #moveCategoryComputer.short_description = u"Переместить в группу Компьютер"

def moveCategoryMfu(ModelAdmin, request, queryset):
    queryset.update(id_type='4')
moveCategoryMfu.short_description = u"Переместить в группу МФУ"

def moveCategorySecurity(ModelAdmin, request, queryset):
    queryset.update(id_type='5')
moveCategorySecurity.short_description = u"Переместить в группу Безопастность"

def moveCategoryUps(ModelAdmin, request, queryset):
    queryset.update(id_type='6')
moveCategoryUps.short_description = u"Переместить в группу ИБП"

def moveCategoryServer(ModelAdmin, request, queryset):
    queryset.update(id_type='7')
moveCategoryServer.short_description = u"Переместить в группу Сервер"

def moveCategoryNd(ModelAdmin, request, queryset):
    queryset.update(id_type='8')
moveCategoryNd.short_description = u"Переместить в группу Сетевое оборудование"

def moveCategoryOther(ModelAdmin, request, queryset):
    queryset.update(id_type='9')
moveCategoryOther.short_description = u"Переместить в группу Разное"

def moveCategoryScaner(ModelAdmin, request, queryset):
    queryset.update(id_type='10')
moveCategoryScaner.short_description = u"Переместить в группу Сканер"

def moveCategoryTerminal(ModelAdmin, request, queryset):
    queryset.update(id_type='11')
moveCategoryTerminal.short_description = u"Переместить в группу Терминал"

def moveCategoryOther(ModelAdmin, request, queryset):
    queryset.update(id_type='9')
moveCategoryOther.short_description = u"Переместить в группу Разное"

def changeStatus(ModelAdmin, request, queryset):
    queryset.update(id_status='4')
changeStatus.short_description = u"Статус - \"Не используется\""


class nameAssets(admin.ModelAdmin):
    list_display = ('id_unit', 'number', 'id_type', 'id_status', 'id_location', 'note')
    exclude = ('a_status',)
    #list_filter = ['']
    search_fields = ['number']
    actions = [changeStatus]

class nameSprType(admin.ModelAdmin):
    list_display = ('id', 'name')

class nameSpr_unit(admin.ModelAdmin):

    list_display = ('name', )
    list_filter = ['id_type']
    search_fields = ['name']
    actions = [moveCategoryMonitor, moveCategoryComputer, moveCategoryMfu, moveCategoryNd,
               moveCategoryOther, moveCategoryPrinter, moveCategoryScaner, moveCategorySecurity,
               moveCategoryServer, moveCategoryTerminal, moveCategoryUps]

admin.site.register(Spr_status)
admin.site.register(Spr_unit, nameSpr_unit)
admin.site.register(Spr_type, nameSprType)
admin.site.register(Spr_location)
admin.site.register(Asset, nameAssets)
admin.site.register(ImageSprUnit)
