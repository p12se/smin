# -*- coding: utf-8 -*-
import datetime
from django.http import HttpResponse
from django.shortcuts import render_to_response
from inventory.models import Spr_unit, Spr_type, Spr_location, Spr_status, Asset, ImageSprUnit
from inventory.forms import FormAsset
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.admin.models import LogEntry
from django.views.decorators.csrf import csrf_exempt
@csrf_exempt

def asset(request):
    types = Spr_type.objects.all()
    assets = Spr_unit.objects.all()
    statuses = Spr_status.objects.all()
    locations = Spr_location.objects.all()

    if 'asset_id' in request.GET:
        asset_id = request.GET['asset_id']
        a = Asset.objects.get(id=asset_id)
        logs = LogEntry.objects.filter(object_id=asset_id)
        images = ImageSprUnit.objects.filter(id_unit=a.id_unit)

        return render_to_response('asset.html', locals())

    if 'saveChanges' in request.GET:
        idAsset = request.GET['hidden_id']
        a = Asset.objects.get(id=idAsset)

        if request.GET['id_status'] != '':
            a.id_status_id = request.GET['id_status']
            a.save()

        if request.GET['id_location'] != '':
            a.id_location_id = request.GET['id_location']
            a.save()

        a.number = request.GET['number']
        a.serial_number = request.GET['serialNumber']
        a.note = request.GET['note']
        a.save()

        return render_to_response('asset.html', locals())

def asset_list(request):
    if 'search' in request.GET:
        search = request.GET['search']
        asset = Asset.objects.order_by("-cdate").filter(number__contains=search)
    else:
        asset = Asset.objects.order_by("-cdate")

    paginator = Paginator(asset, 35)
    page = request.GET.get('page')

    try:
        assets = paginator.page(page)
    except PageNotAnInteger:
        assets = paginator.page(1)
    except EmptyPage:
        assets = paginator.page(paginator.num_pages)

    count_pages = assets.paginator.num_pages
    pages = paginator.page_range
    return render_to_response("list.html", {"assets": assets, "pages": pages})

def asset_add(request):
    assets = Spr_unit.objects.all()
    lastAsset = Asset.objects.filter(a_status=10).order_by('-cdate')[:11]
    types = Spr_type.objects.all()
    locations = Spr_location.objects.all()
    statuss = Spr_status.objects.all()
    asset = Asset.objects.order_by("-cdate")
    count = len(asset)

    if 'type' in request.GET and request.GET['type']:
        atype = request.GET['type']
        aasset = request.GET['asset']
        astatus = request.GET['status']
        alocation = request.GET['location']
        anumber = request.GET['number']
        anote = request.GET['note']
        adate = datetime.datetime.now()
        aa_status = 10
        aprice = None
        complite = True

        a = Asset(id_unit_id=aasset, number=anumber.strip(), id_status_id=astatus, id_type_id=atype,
                  id_location_id=alocation, cdate=datetime.datetime.now(), price=aprice, a_status=aa_status,
                  note=anote.strip())
        a.save()

    return render_to_response('add.html', locals())

def asset_repair(request):
    assets = Asset.objects.filter(id_status_id="2")
    statuses = Spr_status.objects.all()

    if 'r' in request.GET:
        idStatus = request.GET['r']
        idAsset = request.GET['id']
        a = Asset.objects.get(id=idAsset)
        a.id_status_id = idStatus
        a.save()

    return render_to_response('repair.html', locals())

def login(request):
    return render_to_response('login.html', locals())

def report(request):
    if 'r' in request.GET:
        report = request.GET['r']
        if report == '1':
            assets = Asset.objects.order_by('id_location', 'note')
            return render_to_response('report.html', locals())

        if report == '2':
            assets = Asset.objects.order_by('id_location', 'note').filter(id_status_id=4)
            return render_to_response('report.html', locals())

        if report == '3':
            assets = Asset.objects.order_by('id_location', 'note').filter(id_type_id=1)
            return render_to_response('report.html', locals())

