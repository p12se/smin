#-*- coding: utf-8 -*-
from django.db import models


class Spr_status(models.Model):
    name = models.CharField(max_length=20, verbose_name=u'Статус')
    cdate = models.DateTimeField(blank=True)

    class Meta:
        verbose_name = (u'Справочник статусов')
        verbose_name_plural = (u'Справочник статусов')

    def __unicode__(self):
        return unicode(self.name)

class Spr_type(models.Model):
    name = models.CharField(max_length=20, verbose_name=u'Название типа')
    cdate = models.DateTimeField(blank=True)

    class Meta:
        verbose_name = (u'Справочник типов')
        verbose_name_plural = (u'Справочник типов')

    #@property
    def __unicode__(self):
        return unicode(self.name)

class Spr_unit(models.Model):
    name = models.CharField(max_length=255, verbose_name=u'Название актива')
    cdate = models.DateTimeField(blank=True, verbose_name=u'Дата')
    id_type = models.ForeignKey(Spr_type)

    class Meta:
        verbose_name = (u'Справочник активов')
        verbose_name_plural = (u'Справочник активов')

    def __unicode__(self):
        return unicode(self.name)

class Spr_location(models.Model):
    name = models.CharField(max_length=20, verbose_name=u'Название местоположения')
    cdate = models.DateTimeField(blank=True, verbose_name=u'Дата')

    class Meta:
        verbose_name = (u'Справочник местоположений')
        verbose_name_plural = (u'Справочник местоположений')

    def __unicode__(self):
        return unicode(self.name)

class Asset(models.Model):
    id_unit = models.ForeignKey(Spr_unit, verbose_name=u'Актив')
    number = models.CharField(max_length=20, verbose_name=u'Номенклатурный номер')
    serial_number = models.CharField(max_length=50, verbose_name=u'Серийный номер', blank=True)
    id_status = models.ForeignKey(Spr_status, verbose_name=u'Статус')
    id_type = models.ForeignKey(Spr_type, verbose_name=u'Тип')
    id_location = models.ForeignKey(Spr_location, verbose_name=u'Местоположение')
    cdate = models.DateTimeField(blank=True, verbose_name=u'Дата')
    note = models.TextField(max_length=255, verbose_name=u'Примечание', blank=True)
    price = models.DecimalField(max_digits=10,
                                decimal_places=2,
                                verbose_name=u'Цена',
                                blank=True)
    a_status = models.CharField(max_length=2, )


    def chained_relation(self):
        return self.item_set.filter(is_present=True)

    class Meta:
        verbose_name = (u'Актив')
        verbose_name_plural = (u'Активы')

    def __unicode__(self):
        return unicode(self.id_unit)

class ImageSprUnit(models.Model):
    id_unit = models.ForeignKey(Spr_unit,
                                verbose_name=u'Актив')

    image = models.ImageField(upload_to='image/',
                              verbose_name=u'Картинка')

    def __unicode__(self):
        return unicode(self.id_unit)

    class Meta:
        verbose_name = (u'Справочник изображений')
        verbose_name_plural = (u'Справочник изображений')

# class HistoryAsset(models.Model):

