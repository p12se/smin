CREATE DATABASE inventory WITH ENCODING 'UTF-8';
CREATE USER inventory_user WITH PASSWORD '123456';
grant ALL ON DATABASE inventory to inventory_user;