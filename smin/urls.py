from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
from inventory.views import asset, asset_list, asset_add, asset_repair, login, report

urlpatterns = patterns('',
    (r'^asset/', asset),
    (r'^list/', asset_list),
    (r'^add/', asset_add),
    (r'^repair/', asset_repair),
    (r'^login/', login),
    (r'^report/', report),
    #(r'^', login),

    #(r'^assets/', addAsset),
    # Examples:
    # url(r'^$', 'smin.views.home', name='home'),
    # url(r'^smin/', include('smin.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    #url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    #url(r'^chained/', include('chained_selects.urls')),
)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()